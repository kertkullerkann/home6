import java.util.*;


/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();

    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(10, 20); //creates a random graph with n-nodes and m-edges-

        System.out.println(g);
        //g.printOutAdjacencyMatrix();
        //g.printOutTopologicalOrderList();

        System.out.println("This Graph has " + (g.connected.length) + " nodes, named 0 to " + (g.connected.length - 1));
        Scanner scanner = new Scanner(System.in);

        boolean exit = true;
        while (exit) {
            exit = false;
            int from = Integer.MAX_VALUE;
            int to = Integer.MAX_VALUE;

            int count = 0;
            while (!(from <= (g.connected.length - 1) && to <= (g.connected.length - 1))) {
                if (count > 0)
                    System.out.println("Inputs out of Range!!! Try again.");
                System.out.println("Find path from(range 0-" + (g.connected.length - 1) + "): ");
                from = scanner.nextInt();
                System.out.println("Path to(range 0-" + (g.connected.length - 1) + "): ");
                to = scanner.nextInt();
                count++;

            }

            g.printOutLongestPath(from, to);

            System.out.println("Find another longest path? (y - yes) ");
            String exit1 = scanner.next();
            if (exit1.equals("y"))
                exit = true;

            System.out.println();
        }


    }

    /**
     * Design an efficient algorithm for finding a longest directed path from a vertex s
     * to a vertex t of an acyclic weighted digraph . Specify the graph
     * representation used and any auxiliary data structures used. Also, analyze the
     * time complexity of your algorithm.
     */

    static class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;


        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;

        }

        Vertex(String s) {
            this(s, null, null);

        }

        @Override
        public String toString() {
            return id;
        }


    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    static class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int weight;

        Arc(String s, Vertex to, Arc a) {
            id = s;
            target = to;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    static class Graph {

        int numberOfEdges;
        private String id;
        private Vertex first;
        private int info = 0;
        int[][] connected;
        int[] topologicalOrderList;


        /**
         * Graph constructor
         *
         * @param s Graph id
         * @param v First Vertex
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;


        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append("Randomly generated graph with " + this.numberOfEdges + " edges");

            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());

                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());

                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("--w(").append(a.weight).append(")-->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    //sb.append(" Arc weight ").append(a.weight);
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }


            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            Random rand = new Random();
            res.weight = (rand.nextInt(+20) + -10);
            numberOfEdges += 1;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("V" + (n - i - 1));
                if (i > 1) {
                    int vnr = (int) (Math.random() * i);

                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j] = -1 * (a.weight);
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Print out Adjacency matrix.
         */
        public void printOutAdjacencyMatrix() {
            System.out.println("\n Graph adjacency matrix:");
            for (int[] ints : this.connected) {
                System.out.print("[ ");
                for (int i = 0; i < ints.length; i++) {
                    int anInt = ints[i];
                    System.out.print(-1 * (anInt) + " ");
                }
                System.out.print("]");
                System.out.println();
            }
            System.out.println();
        }

        /**
         * Print out longest distances list.
         *
         * @param start Vertex to start from.
         */
        public void printOutLongestDistance(int start) {
            double[] dists = longestPath(start);
            System.out.println("All the longest distances from " + start);
            System.out.println(Arrays.toString(dists));
        }


        /**
         * Method that visits every node.         *
         * https://github.com/williamfiset/Algorithms/blob/master/com/
         * williamfiset/algorithms/graphtheory/TopologicalSortAdjacencyMatrix.java
         *
         * @param adj     Adjacency matrix.
         * @param visited Visited list
         * @param order   Topological order list
         * @param index   Index for the node
         * @return node index
         */

        private int visit(int[][] adj, boolean[] visited, int[] order, int index, int u) {

            if (visited[u]) return index;
            visited[u] = true;

            // Visit all neighbors
            for (int v = 0; v < adj.length; v++)
                if (adj[u][v] != 0 && !visited[v])
                    index = visit(adj, visited, order, index, v);

            // Place this node at the head of the list
            order[index--] = u;

            return index;
        }

        /**
         * Sort graph vertices in linear order.
         * https://github.com/williamfiset/Algorithms/blob/master/com/
         * williamfiset/algorithms/graphtheory/TopologicalSortAdjacencyMatrix.java
         *
         * @param adj Adjacency matrix
         * @return Topological order list.
         */
        public int[] topologicalSort(int[][] adj) {


            int n = adj.length;
            boolean[] visited = new boolean[n];
            int[] order = new int[n];
            int index = n - 1;

            // Visit each node
            for (int u = 0; u < n; u++)
                if (!visited[u])
                    index = visit(adj, visited, order, index, u);


            return order;
        }

        /**
         * Print out sorted list in.
         */
        public void printOutTopologicalOrderList() {
            System.out.println(" \nTopological sort\n" + Arrays.toString(this.topologicalOrderList) + "\n");
        }


        /**
         * Print out longest path from node a to node b
         *
         * @param from start node
         * @param to   destination node
         */
        public void printOutLongestPath(int from, int to) {
            long startTime = System.currentTimeMillis();
            double[] longest = longestPath(from);
            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            if (longest[to] != Double.POSITIVE_INFINITY) {
                System.out.println("Longest path from " + from + " to " + to + " is " + (-1 * (longest[to])));
                System.out.println("Time elapsed " + elapsedTime + " milliseconds");
            } else {
                System.out.println("There is no path from " + from + " to " + to);
            }
        }


        /**
         * Find all the longest paths from start node to any possible destination node
         *
         * @param start node where to start from.
         * @return list of longest paths
         */
        public double[] longestPath(int start) {

            // Set up array used to maintain minimum distances from start
            int n = this.connected.length;
            double[] dist = new double[n];

            //Fill all the distances with infinity first.
            java.util.Arrays.fill(dist, Double.POSITIVE_INFINITY);
            //Set the starting vertex distance to zero.
            dist[start] = 0.0;
            // Process nodes in a topologically sorted order
            int[] topologicalSort = topologicalSort(this.connected);
            for (int i = 0; i < topologicalSort.length; i++) {
                int u = topologicalSort[i];
                for (int v = 0; v < n; v++) {
                    if (this.connected[u][v] != 0) {
                        double newDist = dist[u] + (this.connected[u][v]);
                        if (newDist < dist[v]) {
                            dist[v] = newDist;
                        }
                    }
                }
            }

            // Return maximum list.
            return dist;

        }

        /**
         * Create directed acyclic weighted (directed, weighted, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            this.connected = createAdjMatrix();
            int edgeCount = m - n + 2;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                if (i < j) {
                    Vertex vi = vert[i];
                    Vertex vj = vert[j];
                    Arc arc = createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                    connected[i][j] = -1 * arc.weight;
                    edgeCount--;
                }// a new edge happily created}

            }
            this.topologicalOrderList = (topologicalSort(connected));
        }


    }
}

//Had ideas and used tips from sites:

//https://mathematica.stackexchange.com/questions/608/how-to-generate-random-directed-acyclic-graphs
//https://algorithmsinsight.wordpress.com/graph-theory-2/
//https://github.com/williamfiset/Algorithms/blob/master/com/williamfiset/algorithms/graphtheory/TopologicalSortAdjacencyMatrix.java
//https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
//https://medium.com/basecs/spinning-around-in-cycles-with-directed-acyclic-graphs-a233496d4688
//https://algorithmsinsight.wordpress.com/graph-theory-2/
//http://ww3.algorithmdesign.net/sample/ch07-weights.pdf
//https://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/

